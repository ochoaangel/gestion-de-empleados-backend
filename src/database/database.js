const mariadb = require("mariadb");
const pool = mariadb.createPool({
  host: "127.0.0.1",
  user: "root",
  port: "3306",
  password: "mypassword",
  database: "gestionempleados",
  acquireTimeout: 1000000,
});

async function getConnection() {
  try {
    return await pool.getConnection();
  } catch (error) {
    console.log("ERROR al conectarse a la BD", error);
  }
}

module.exports = { getConnection };
