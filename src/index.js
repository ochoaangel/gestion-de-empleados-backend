const app = require("express")();
const http = require("http").createServer(app);
const bodyParser = require("body-parser");
const pool = require("./database/database");
var moment = require("moment");
var cors = require("cors");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

/////////////////////////////////////////////////////
//  Endpoint para obtener todos los datos de la BD //
/////////////////////////////////////////////////////
app.get("/getEmployees", async function (req, res) {
  let myquery = `SELECT * FROM employee`;
  let resp = await ejecutar(myquery);
  res.send(resp);
});

///////////////////////////////////////////////////////////
//  Endpoint para obtener máximo 3 salarios consecutivos //
///////////////////////////////////////////////////////////
app.get("/getEmployeeSalary", async function (req, res) {
  if (!req.headers.code) {
    res.send([]);
    console.log("ERROR: no se recibe el valor de código en la cabecera");
  }
  let idEmployee = req.headers.code;
  let myquery = `SELECT * FROM employee WHERE EmployeeCode = '${idEmployee}'`;
  let resp = await ejecutar(myquery);

  res.send(resp);
});

///////////////////////////////////////////////////////////
//  Endpoint para agregar o insertar salario de empleado //
///////////////////////////////////////////////////////////
app.post("/setEmployee", async function (req, res) {
  let {
    Year,
    Month,
    Office,
    EmployeeCode,
    EmployeeName,
    EmployeeSurname,
    Division,
    Position,
    Grade,
    BeginDate,
    Birthday,
    Identification,
    BaseSalary,
    ProductionBonus,
    CompensationBonus,
    Commission,
    Contributions, 
  } = req.body;

  let myquery = `INSERT INTO Employee ( Year, Month, Office, EmployeeCode, EmployeeName, EmployeeSurname, Division, Position, Grade, BeginDate, Birthday, Identification, BaseSalary, ProductionBonus, CompensationBonus, Commission, Contributions)
  VALUES
  ( "${Year}", "${Month}", "${Office}", "${EmployeeCode}", "${EmployeeName}", "${EmployeeSurname}", "${Division}", "${Position}", "${Grade}", "${BeginDate}", "${Birthday}", "${Identification}", "${BaseSalary}", "${ProductionBonus}", "${CompensationBonus}", "${Commission}", "${Contributions}") ;`;
  let resp = await ejecutar(myquery);
  res.send(resp);
}); 

/////////////////////////////////////////////////////
//  Endpoint para obtener todos los datos de la BD //
/////////////////////////////////////////////////////
app.get("/getDivisions", async function (req, res) {
  let myquery = `SELECT * FROM division`;
  let resp = await ejecutar(myquery);
  res.send(resp);
});

/////////////////////////////////////////////////////
//  Endpoint para obtener todos los datos de la BD //
/////////////////////////////////////////////////////
app.get("/getOffices", async function (req, res) {
  let myquery = `SELECT * FROM office`;
  let resp = await ejecutar(myquery);
  res.send(resp);
});

/////////////////////////////////////////////////////
//  Endpoint para obtener todos los datos de la BD //
/////////////////////////////////////////////////////
app.get("/getPositions", async function (req, res) {
  let myquery = `SELECT * FROM position`;
  let resp = await ejecutar(myquery);
  res.send(resp);
});

http.listen(3000, () => {
  console.log("listening on *:3000");
});

async function ejecutar(myquery) {
  try {
    let conn = await pool.getConnection();
    setTimeout(() => {
      conn.end();
    }, 1000);
    return await conn.query(myquery);
  } catch (e) {
    console.log("ERROR en la consulta a la BD :", e);
  }
}
