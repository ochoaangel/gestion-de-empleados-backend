//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////  Importaciones Necesarias /////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
const faker = require("faker/locale/es");
const fs = require("fs");
var moment = require("moment");
moment.locale("es");

//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////  Definiendo Variables Importantes /////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

let cantidadDeEmpleados = 150;

let referenceSalary = [
  { grado: 6, salarioBase: 2800 },
  { grado: 8, salarioBase: 3500 },
  { grado: 10, salarioBase: 4000 },
  { grado: 12, salarioBase: 4500 },
  { grado: 14, salarioBase: 4580 },
  { grado: 16, salarioBase: 4700 },
  { grado: 18, salarioBase: 5000 },
];

let optOffice15 = [
  "A",
  "B",
  "C",
  "D",
  "H",
  "ZZ",
  "RH",
  "MG",
  "G",
  "AA",
  "YU",
  "CO",
  "PE",
  "US",
  "DE",
];

//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////  Agregando Tablas /////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

let crearTablas = `
DROP TABLE IF EXISTS Employee;
CREATE TABLE Employee (
    \`Id\`                  BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    \`Year\`                SMALLINT,
    \`Month\`               TINYINT,
    \`Office\`              varchar(10),
    \`EmployeeCode\`        varchar(10) ,
    \`EmployeeName\`        varchar(150),
    \`EmployeeSurname\`     varchar(150),
    \`Division\`            varchar(150),  
    \`Position\`            varchar(150),
    \`Grade\`               TINYINT,
    \`BeginDate\`           varchar(30),
    \`Birthday\`            varchar(30),
    \`Identification\`      varchar(10), 
    \`BaseSalary\`          DECIMAL,
    \`ProductionBonus\`     DECIMAL,
    \`CompensationBonus\`   DECIMAL,
    \`Commission\`          DECIMAL,
    \`Contributions\`       DECIMAL,
    PRIMARY KEY (Id)
);


DROP TABLE IF EXISTS Office;
CREATE TABLE Office (
    \`Id\`            BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,    
    \`Name\`        	varchar(50) ,
    \`Code\`        	varchar(20),
    \`Description\`   varchar(300),
    PRIMARY KEY (Id)
);


DROP TABLE IF EXISTS Division;
CREATE TABLE Division (
    \`Id\`            BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,    
    \`Name\`        	varchar(50) ,
    \`Code\`        	varchar(20),
    \`Description\`   varchar(300),
    PRIMARY KEY (Id)
);

DROP TABLE IF EXISTS Position;
CREATE TABLE Position (
    \`Id\`            BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,    
    \`Name\`        	varchar(50) ,
    \`Code\`        	varchar(20),
    \`Description\`   varchar(300),
    PRIMARY KEY (Id)
);


`;

///////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////  Llenando Tabla Empleados  /////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
let textEmpleados = `
INSERT INTO Employee ( Year, Month, Office, EmployeeCode, EmployeeName, EmployeeSurname, Division, Position, Grade, BeginDate, Birthday, Identification, BaseSalary, ProductionBonus, CompensationBonus, Commission, Contributions)
VALUES \n`;

let i = 0;
while (i < cantidadDeEmpleados) {
  let Year = "2020";
  let Month = getRandomInt(1, 12);
  let Office = optOffice15[getRandomInt(0, 14)];
  let EmployeeCode = "1" + getRandomInt(0000000, 9999999);
  let EmployeeName = faker.name.findName();
  let EmployeeSurname = faker.name.lastName();
  let Division = faker.name.jobArea();
  let Position = faker.name.jobType();
  let Grade = getRandomInt(6, 18);
  let BeginDate = moment(faker.date.past()).format("MMMM DD, YYYY");
  let Birthday = moment(faker.date.past()).format("MMMM DD, YYYY");
  let Identification = getRandomInt(0, 50000000);
  let BaseSalary = faker.name.findName();
  let ProductionBonus = getRandomInt(0, 3000);
  let CompensationBonus = getRandomInt(0, 5000);
  let Commission = getRandomInt(0, 5000);
  let Contributions = getRandomInt(0, 1000);

  let acumuladoMes = [];
  for (let ifor = 0; ifor < getRandomInt(1, 12); ifor++) {
    let currentMonth = getRandomInt(1, 12);

    while (acumuladoMes.includes(currentMonth)) {
      currentMonth = getRandomInt(1, 12);
    }
    acumuladoMes.push(currentMonth);
  }

  acumuladoMes.forEach((mes) => {
    let aleatorio = getRandomInt(0, 6);
    Grade = referenceSalary[aleatorio]["grado"];
    BaseSalary = referenceSalary[aleatorio]["salarioBase"];

    let insertEmpleado = ` 
    ( "${Year}", "${mes}", "${Office}", "${EmployeeCode}", "${EmployeeName}", "${EmployeeSurname}", "${Division}", "${Position}", "${Grade}", "${BeginDate}", "${Birthday}", "${Identification}", "${BaseSalary}", "${ProductionBonus}", "${CompensationBonus}", "${Commission}", "${Contributions}") ,`;
    textEmpleados = textEmpleados + insertEmpleado;
    i++;
  });
}

//////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////  Llenando Tabla Office  //////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
let textOffice = `\n
INSERT INTO Office ( Name, Code, Description ) VALUES `;

let jobAreaAll = [];

for (let index = 0; index < optOffice15.length * 10; index++) {
  jobAreaAll.push(faker.name.jobArea());
}

let jobArea = jobAreaAll.filter((item, index, array) => {
  return array.indexOf(item) === index;
});

jobArea = jobArea.slice(0, optOffice15.length);

for (let index = 0; index < optOffice15.length; index++) {
  textOffice =
    textOffice +
    `
    ( "${jobArea[index]}" , "${
      optOffice15[index]
    }" ,"${faker.lorem.sentence()}"  ),`;
}

//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////  Llenando Tabla Division  /////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
let textDivision = `\n
INSERT INTO Division ( Name, Code, Description ) VALUES `;

jobAreaAll = [];

for (let index = 0; index < optOffice15.length * 10; index++) {
  jobAreaAll.push(faker.name.jobType());
}

jobArea = jobAreaAll.filter((item, index, array) => {
  return array.indexOf(item) === index;
});

jobArea = jobArea.slice(0, optOffice15.length);

for (let index = 0; index < optOffice15.length; index++) {
  textDivision =
    textDivision +
    `
    ( "${jobArea[index]}" , "${getRandomInt(
      2400,
      2499
    )}" ,"${faker.lorem.sentence()}"  ),`;
}

//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////  Llenando Tabla Position  /////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
let textPosition = `\n
INSERT INTO Position ( Name, Code, Description ) VALUES `;

jobAreaAll = [];

for (let index = 0; index < optOffice15.length * 10; index++) {
  jobAreaAll.push(faker.name.jobTitle());
}

jobArea = jobAreaAll.filter((item, index, array) => {
  return array.indexOf(item) === index;
});

jobArea = jobArea.slice(0, optOffice15.length);

for (let index = 0; index < optOffice15.length; index++) {
  textPosition =
    textPosition +
    `
    ( "${jobArea[index]} ", "${getRandomInt(
      50,
      99
    )}" ,"${faker.lorem.sentence()}"  ),`;
}
//////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////  Uniendo todo en 'end'  //////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
end =
  crearTablas +
  textEmpleados.slice(0, -1) +
  ";" +
  textOffice.slice(0, -1) +
  ";" +
  textDivision.slice(0, -1) +
  ";" +
  textPosition.slice(0, -1) +
  ";";
+";";
//////////////////////////////////////////////////////////////////////////////////////
////////////////////////////  Generando el archivo Script  ///////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
fs.writeFileSync("SQLscriptGENERATED.sql", end);

console.log("► ► ► ► Generado Script para la Base de Datos SQL..");

//////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////  Funciones útiles  ////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
/**
 *  Genera un número aleatorio entre el numero minimo y máximo
 * @param {*} min
 * @param {*} max
 * @returns  number between min and máx
 */
function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
